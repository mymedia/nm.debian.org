from django.core.management.base import BaseCommand
import backend.utils as utils

import json
import gzip
import logging

log = logging.getLogger(__name__)

class Command(BaseCommand):
    help = "change the status of a person"

    def add_arguments(self, parser):
        parser.add_argument("--full", action="store_true", help="Full DB backup or not")
        parser.add_argument("--path", action="store", type=str, help="The path where to do the backup")

    def handle(self, **opts):
        from backend.export import export_db

        if opts["path"] is None:
            log.info("Backup path is not set: skipping backups")
            return
        fname = opts["path"]

        log.info("backup_cmd: serializing the Database (full: %s)", opts["full"])
        exported = export_db(full=opts["full"])

        class Serializer(json.JSONEncoder):
            def default(self, o):
                if hasattr(o, "strftime"):
                    return o.strftime("%Y-%m-%d %H:%M:%S")
                return json.JSONEncoder.default(self, o)

        # Base filename for the backup
        log.info("backup_cmd: backing up to %s", fname)

        # Write the backup file
        with utils.atomic_writer(fname, mode="wb", chmod=0o640) as fd:
            with gzip.open(fd, mode="wt", compresslevel=9) as gzfd:
                json.dump(exported, gzfd, cls=Serializer, indent=2)
