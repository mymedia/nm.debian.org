"""
Code used to access Debian's LDAP
"""
from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.urls import reverse
from django.forms.models import model_to_dict
from django.core.exceptions import ValidationError
from backend.models import Person, PersonAuditLog

#  enrico> For people like Wookey, do you prefer we use only cn or only sn?
#          "sn" is used currently, and "cn" has a dash, but rather than
#          cargo-culting that in the new NM double check it with you
# @sgran> cn would be more usual
# @sgran> cn is the "whole name" and you can split it up into givenName + sn if you like
#  phil> Except that in Debian LDAP it isn't.
#  enrico> sgran: ok. should I use 'cn' for potential new cases then?
# @sgran> phil: indeed
# @sgran> but if we keep doing it the other way, we'll never be in a position to change
# @sgran> enrico: please
#  enrico> sgran: ack


class LDAPFieldsManager(models.Manager):
    def create(self, **kw):
        """
        Create a new process and all its requirements
        """
        audit_author = kw.pop("audit_author", None)
        audit_notes = kw.pop("audit_notes", None)
        audit_skip = kw.pop("audit_skip", False)

        obj = self.model(**kw)
        obj.save(using=self._db, audit_author=audit_author, audit_notes=audit_notes, audit_skip=audit_skip)
        return obj


class LDAPFields(models.Model):
    person = models.OneToOneField(Person, null=False, related_name="ldap_fields", on_delete=models.CASCADE)
    cn = models.CharField(_("first name"), max_length=250, null=False)
    mn = models.CharField(_("middle name"), max_length=250, null=False, blank=True, default="")
    sn = models.CharField(_("last name"), max_length=250, null=False, blank=True, default="")
    email = models.EmailField(_("LDAP forwarding email address"), null=False, blank=True)
    uid = models.CharField(_("Debian account name"), max_length=32, unique=True, null=True, blank=True)

    objects = LDAPFieldsManager()

    def __str__(self):
        return " ".join(x for x in (self.cn, self.mn, self.sn) if x) + f" <{self.uid}>"

    def get_absolute_url(self):
        return reverse("person:show", kwargs={"key": self.person.lookup_key})

    def clean_fields(self, exclude=None, *args, **kw):
        dam_forbidden_uids = (
            # standard system users
            "root", "daemon", "bin", "sys", "games", "man", "news", "uucp",
            "proxy", "www", "backup", "list", "irc", "gnats",
            # special Debian users
            "dsa", "dam", "dpl",
        )
        dsa_forbidden_uids = (
            # even if they are not taken, DSA doesn't like them
            "dns",
        )
        super().clean_fields(exclude=exclude, *args, **kw)
        if not exclude or "uid" not in exclude:
            if not self.uid:
                self.uid = None

            if self.uid and self.uid.endswith("-guest"):
                raise ValidationError({
                    "uid": _("uid must not end in -guest"),
                })
            if self.uid and len(self.uid) < 3:
                raise ValidationError({
                    "uid": _("uid must be at least 3 characters long"),
                })
            if self.uid and not self.uid.islower():
                raise ValidationError({
                    "uid": _("uid must be made of only lowercase characters"),
                })
            if self.uid and "." in self.uid:
                raise ValidationError({
                    "uid": _("DSA expressed a preference against dots in usernames."
                             " Contact debian-admin@lists.debian.org for details."),
                })
            if self.uid and self.uid in dam_forbidden_uids:
                raise ValidationError({
                    "uid": _("The requested uid is a system user or a well known alias,"
                             " and cannot be used."),
                })
            if self.uid and self.uid in dsa_forbidden_uids:
                raise ValidationError({
                    "uid": _("The requested uid is not accepted by DSA."
                             " Contact debian-admin@lists.debian.org for details."),
                })

    def save(self, *args, **kw):
        """
        Save, and add an entry to the Person audit log.

        Extra arguments that can be passed:

            audit_author: Person instance of the person doing the change
            audit_notes: free form text annotations for this change
            audit_skip: skip audit logging, used only for tests

        """
        # Extract our own arguments, so that they are not passed to django
        author = kw.pop("audit_author", None)
        notes = kw.pop("audit_notes", "")
        audit_skip = kw.pop("audit_skip", False)

        if audit_skip:
            changes = None
        else:
            # Get the previous version of the Person object, so that PersonAuditLog
            # can compute differences
            if self.pk:
                old_ldap_fields = LDAPFields.objects.get(pk=self.pk)
            else:
                old_ldap_fields = None

            changes = LDAPFields.diff(old_ldap_fields, self)
            if changes and not author:
                raise RuntimeError("Cannot save a LDAPFields instance without providing Author information")

        # Perform the save; if we are creating a new person, this will also
        # fill in the id/pk field, so that PersonAuditLog can link to us
        super().save(*args, **kw)

        # Finally, create the audit log entry
        if changes:
            PersonAuditLog.objects.create(
                    person=self.person, author=author, notes=notes, changes=PersonAuditLog.serialize_changes(changes))

    @classmethod
    def diff(cls, old_obj: "LDAPFields", new_obj: "LDAPFields"):
        """
        Compute the changes between two different instances of a Person model
        """
        changes = {}
        if old_obj is None:
            for k, nv in list(model_to_dict(new_obj).items()):
                changes[k] = [None, nv]
        else:
            old = model_to_dict(old_obj)
            new = model_to_dict(new_obj)
            for k, nv in list(new.items()):
                ov = old.get(k, None)
                # Also ignore changes like None -> ""
                if ov != nv and (ov or nv):
                    changes[k] = [ov, nv]
        return changes
