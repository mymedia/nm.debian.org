from django.test import TestCase
from django.urls import reverse
from backend.unittest import BaseFixtureMixin, NamedObjects
from backend import const
from apikeys.models import Key


class TestStatusAllPermissions(BaseFixtureMixin, TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.add_named_objects(keys=NamedObjects(Key))

        p = cls.create_person("dc", status=const.STATUS_DC, alioth=True)
        username = "dc@users.alioth.debian.org"
        cls.identities.create(
                "dc_debsso", person=p, issuer="debsso", subject=username, username=username, audit_skip=True)
        cls.keys.create("dc", user=p, name="test", value="testkey", enabled=True)

        p = cls.create_person("dd_nu", status=const.STATUS_DD_NU)
        username = "dd_nu@debian.org"
        cls.identities.create(
                "dd_nu_debsso", person=p, issuer="debsso", subject=username, username=username, audit_skip=True)

    def test_anonymous(self):
        client = self.make_test_client(None)
        self.assertGetAllForbidden(client)
        self.assertGetByStatusForbidden(client)
        self.assertGetNamedAllowed(client)

    def test_loggedin(self):
        client = self.make_test_client("dc")
        self.assertGetAllAllowed(client)
        self.assertGetByStatusAllowed(client)
        self.assertGetNamedAllowed(client)

    def test_apitoken(self):
        client = self.make_test_client(None)
        client.defaults["HTTP_API_KEY"] = self.keys.dc.value
        self.assertGetAllAllowed(client)
        self.assertGetByStatusAllowed(client)
        self.assertGetNamedAllowed(client)

    def assertGetAllAllowed(self, client):
        response = client.get(reverse("api:status"))
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(response.content, {
            'people': {
                'dc@users.alioth.debian.org': {'status': 'dc'},
                'dd_nu@debian.org': {'status': 'dd_nu'},
            }
        })

    def assertGetAllForbidden(self, client):
        response = client.get(reverse("api:status"))
        self.assertPermissionDenied(response)

    def assertGetByStatusAllowed(self, client):
        response = client.get(reverse("api:status"), data={"status": "dd_u,dd_nu"})
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(response.content, {
            'people': {'dd_nu@debian.org': {'status': 'dd_nu'}},
        })

    def assertGetByStatusForbidden(self, client):
        response = client.get(reverse("api:status"), data={"status": "dd_u,dd_nu"})
        self.assertPermissionDenied(response)

    def assertGetNamedAllowed(self, client):
        response = client.get(reverse("api:status"), data={"person": "dd_nu@debian.org"})
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(response.content, {
            'people': {'dd_nu@debian.org': {'status': 'dd_nu'}},
        })

        response = client.post(
                reverse("api:status"),
                data='["dd_nu@debian.org","dc@users.alioth.debian.org"]',
                content_type="application/json")
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(response.content, {
            'people': {
                'dc@users.alioth.debian.org': {'status': 'dc'},
                'dd_nu@debian.org': {'status': 'dd_nu'}
            },
        })
