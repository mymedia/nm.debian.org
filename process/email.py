from backend import const
from backend.shortcuts import build_absolute_uri
from django.utils.timezone import now
from django.contrib.sites.models import Site
import logging
from nm2.lib.email import build_django_message

log = logging.getLogger(__name__)


def notify_new_process(process, request=None):
    """
    Render a notification email template for a newly uploaded statement, then
    send the resulting email.
    """
    if request is None:
        url = "https://{}{}".format(
            Site.objects.get_current().domain,
            process.get_absolute_url())
    else:
        url = build_absolute_uri(process.get_absolute_url(), request)

    body = """Hello,

you have just started a new process to become {applying_for}.

The nm.debian.org page for managing this process is at {url}

That page lists several requirements that need to be fulfilled for this process
to complete. Some of those you can provide yourself: look at the page for a
list and some explanation.

I hope you have a smooth process, and if you need anything please mail
nm@debian.org.


Yours,
the nm.debian.org housekeeping robot
"""
    body = body.format(
        applying_for=const.ALL_STATUS_DESCS[process.applying_for], process=process, url=url)

    msg = build_django_message(
        from_email=("nm.debian.org", "nm@debian.org"),
        to=process.person,
        cc=process.archive_email,
        subject="New Member process, {}".format(
            const.ALL_STATUS_DESCS[process.applying_for]),
        body=body)
    msg.send()
    log.debug("sent mail from %s to %s cc %s bcc %s subject %s",
              msg.from_email,
              ", ".join(msg.to),
              ", ".join(msg.cc),
              ", ".join(msg.bcc),
              msg.subject)


def notify_new_statement(statement, request=None, cc_nm=True, notify_ml="newmaint", mia=None, date=None):
    """
    Render a notification email template for a newly uploaded statement, then
    send the resulting email.
    """
    if date is None:
        date = now()
    process = statement.requirement.process

    if request is None:
        url = "https://{}{}".format(
            Site.objects.get_current().domain,
            process.get_absolute_url())
    else:
        url = build_absolute_uri(process.get_absolute_url(), request)

    body = """{statement.statement}

{statement.uploaded_by.fullname} (via nm.debian.org)

For details and to comment, visit {url}
"""
    body += "-- \n"
    body += "{url}\n"
    body = body.format(statement=statement, url=url)

    cc = [process.person, process.archive_email]
    if cc_nm:
        cc.append("nm@debian.org")

    if process.person.lookup_key != statement.uploaded_by.lookup_key:
        cc.append(statement.uploaded_by)

    headers = {}
    if mia is not None:
        headers["X-MIA-Summary"] = mia
        cc.append("mia-{}@qa.debian.org".format(process.person.ldap_fields.uid))

    # for Declaration of Intent adding target of process to mail subject
    if statement.requirement.type == "intent":
        extra_info = f" to become a {const.ALL_STATUS_BYTAG[process.applying_for].sdesc}"
    else:
        extra_info = ""

    msg = build_django_message(
        from_email=(f"{statement.uploaded_by.fullname} (via nm.debian.org)", "nm@debian.org"),
        to="debian-{}@lists.debian.org".format(notify_ml),
        cc=cc,
        subject="{}: {}{}".format(
            process.person.fullname,
            statement.requirement.type_desc,
            extra_info),
        date=date,
        headers=headers,
        reply_to=[(f"{statement.uploaded_by.fullname}", statement.uploaded_by.email),],
        body=body)
    msg.send()
    log.debug("sent mail from %s to %s cc %s bcc %s subject %s",
              msg.from_email,
              ", ".join(msg.to),
              ", ".join(msg.cc),
              ", ".join(msg.bcc),
              msg.subject)


def notify_new_log_entry(entry, request=None, mia=None):
    """
    Render a notification email template for a newly uploaded process log
    entry, then send the resulting email.
    """
    process = entry.process

    url = build_absolute_uri(process.get_absolute_url(), request)

    body = """{entry.logtext}

{entry.changed_by.fullname} (via nm.debian.org)
"""
    body += "-- \n"
    body += "{url}\n"
    body = body.format(entry=entry, url=url)

    if entry.is_public:
        cc = [process.person, process.archive_email]
        subject = "{}: new public log entry".format(process.person.fullname)
    else:
        cc = []
        subject = "{}: new private log entry".format(process.person.fullname)

    if not entry.is_public or process.person.lookup_key != entry.changed_by.lookup_key:
        cc.append(entry.changed_by)

    headers = {}
    if mia is not None:
        headers["X-MIA-Summary"] = mia
        cc.append("mia-{}@qa.debian.org".format(process.person.ldap_fields.uid))

    msg = build_django_message(
        from_email=(f"{entry.changed_by.fullname} (via nm.debian.org)", "nm@debian.org"),
        to="nm@debian.org",
        cc=cc,
        subject=subject,
        headers=headers,
        reply_to=[(f"{entry.changed_by.fullname}", entry.changed_by.email),],
        body=body)
    msg.send()
    log.debug("sent mail from %s to %s cc %s bcc %s subject %s",
              msg.from_email,
              ", ".join(msg.to),
              ", ".join(msg.cc),
              ", ".join(msg.bcc),
              msg.subject)


def notify_am_assigned(assignment, request=None):
    """
    Render a notification email template for an AM assignment, then send the
    resulting email.
    """
    process = assignment.process

    if request is None:
        url = "https://{}{}".format(
            Site.objects.get_current().domain,
            process.get_absolute_url())
    else:
        url = build_absolute_uri(process.get_absolute_url(), request)

    body = """Hello,

{process.person.fullname} meet {am.person.fullname}, your new application manager.
{am.person.fullname} meet {process.person.fullname}, your new applicant.

The next step is usually one of these:
- the application manager sends an email to the applicant starting a
conversation;
- the application manager has no time and can go to {url}/am_ok
to undo the assignment.

The nm.debian.org page for this process is at {url}

I hope you have a good time, and if you need anything please mail nm@debian.org.

{assignment.assigned_by.fullname} for Front Desk
"""

    body = body.format(process=process, am=assignment.am.person,
                       assignment=assignment, url=url)

    msg = build_django_message(
        (f"{assignment.assigned_by.fullname} (via nm.debian.org)", "nm@debian.org"),
        to=[process.person, assignment.am.person],
        cc=[process.archive_email, assignment.assigned_by],
        subject="New Member process, {}".format(
            const.ALL_STATUS_DESCS[process.applying_for]),
        reply_to=[(f"{assignment.assigned_by.fullname}", assignment.am.person.email),],
        body=body)
    msg.send()
    log.debug("sent mail from %s to %s cc %s bcc %s subject %s",
              msg.from_email,
              ", ".join(msg.to),
              ", ".join(msg.cc),
              ", ".join(msg.bcc),
              msg.subject)


def notify_new_dd(process, request=None):
    """
    Render a notification email template to let leader@debian.org know of new
    DDs, then send the resulting email.
    """
    if request is None:
        url = "https://{}{}".format(
            Site.objects.get_current().domain,
            process.get_absolute_url())
    else:
        url = build_absolute_uri(process.get_absolute_url(), request)

    body = """Hello,

{process.person.fullname} <{process.person.ldap_fields.uid}> has just become {status}.

The nm.debian.org page for this process is at {url}

Debian New Member Front Desk
"""

    body = body.format(
        process=process, status=const.ALL_STATUS_DESCS_WITH_PRONOUN[process.applying_for], url=url)

    msg = build_django_message(
        from_email=("nm.debian.org", "nm@debian.org"),
        to=["leader@debian.org"],
        subject="New {}: {} <{}>".format(
            const.ALL_STATUS_DESCS[process.applying_for], process.person.fullname, process.person.ldap_fields.uid),
        body=body)
    msg.send()
    log.debug("sent mail from %s to %s cc %s bcc %s subject %s",
              msg.from_email,
              ", ".join(msg.to),
              ", ".join(msg.cc),
              ", ".join(msg.bcc),
              msg.subject)


def ping_process(pinger, process, message=None, request=None):
    """
    Render a notification email template for pinging a stuck process, then send
    the resulting email.
    """
    if request is None:
        url = "https://{}{}".format(
            Site.objects.get_current().domain,
            process.get_absolute_url())
    else:
        url = build_absolute_uri(process.get_absolute_url(), request)

    format_args = {
        "process": process,
        "pinger": pinger,
        "url": url,
    }

    body = ["""Hello,

the process at {url} looks stuck.
""".format(**format_args)]

    if message:
        body.append(message)

    body.append("""
If you need help with anything, please mail nm@debian.org.

{pinger.fullname} for Front Desk
""".format(**format_args))

    to = [process.person]
    assignment = process.current_am_assignment
    if assignment:
        to.append(assignment.am.person)

    msg = build_django_message(
        pinger,
        to=to,
        cc=[process.archive_email, "nm@debian.org"],
        subject="NM process stuck?",
        body="".join(body))
    msg.send()
    log.debug("sent mail from %s to %s cc %s bcc %s subject %s",
              msg.from_email,
              ", ".join(msg.to),
              ", ".join(msg.cc),
              ", ".join(msg.bcc),
              msg.subject)
