from __future__ import annotations
from django.utils.translation import ugettext as _
from django.core.exceptions import PermissionDenied
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils.timezone import now
from django.utils.html import format_html, mark_safe, escape
from backend.mixins import VisitPersonMixin
from nmlayout.mixins import NavLink
from . import models as pmodels


def compute_process_status(process, visitor, visit_perms=None):
    """
    Return a dict with the process status:
    {
        "requirements_ok": [list of Requirement],
        "requirements_missing": [list of Requirement],
        "log_first": Log,
        "log_last": Log,
    }
    """
    from process.models import REQUIREMENT_TYPES_DICT
    from process.permissions import ProcessVisitorPermissions
    # person_perms = visit_perms
    if visit_perms and isinstance(visit_perms, ProcessVisitorPermissions):
        process_perms = visit_perms
    else:
        process_perms = None
    rok = []
    rnok = []
    rapproval = None
    requirements = {}
    for r in process.requirements.all():
        if r.type == "approval":
            rapproval = r
        if r.approved_by:
            rok.append(r)
        else:
            rnok.append(r)
        requirements[r.type] = r

    # Compute the list of advocates
    adv = requirements.get("advocate", None)
    advocates = set()
    if adv is not None:
        for s in adv.statements.all():
            advocates.add(s.uploaded_by)

    view_private_logs = False
    if not visitor.is_authenticated:
        pass
    elif visitor.is_superuser:
        view_private_logs = True
    elif process_perms is None:
        process_perms = process.permissions_of(visitor)
        if "view_private_log" in process_perms:
            view_private_logs = True

    log = process.log.order_by("logdate").select_related(
        "changed_by", "requirement")
    if not view_private_logs:
        from django.db.models import Q
        q = Q(is_public=True)
        if visitor.is_authenticated:
            q = q | Q(changed_by=visitor)
        log = log.filter(q)

    log = list(log)

    am_assignment = process.current_am_assignment

    if process.closed:
        summary = _("Closed")
        if process.rt_ticket is not None:
            rt_link = mark_safe(f'<a href="https://rt.debian.org/{process.rt_ticket}">#{process.rt_ticket}</a>')
            summary_html = format_html(_("Closed. RT ticket: {}"), rt_link)
        else:
            summary_html = escape(summary)
    elif process.approved_by:
        summary_bits = []

        # A process should be frozen before its approval!!
        if not process.frozen_by:
            summary_bits.append(_("Process should have been frozen before approval!!"))

        # If a RT ticket is opened or if DAM has approved, then it's approved, end of story.
        if process.rt_ticket is not None or process.has_dam_approval():
            summary = _("Approved")
            summary_bits.append(_("Approved by {} on {}."))
        # FD approval here
        else:
            (latest_comment_date, first_next_statement_date) = process.latest_approval_activities()
            # A DD comment in the approval statement stops the clock
            if (latest_comment_date is not None and
               (first_next_statement_date is None or
                   latest_comment_date > first_next_statement_date)):
                summary = _("Approval needs review")
                summary_bits.append(_("A review of {}'s approval on {} is needed."))
            else:
                summary = _("FD Approved")
                summary_bits.append(_("FD Approved by {} on {}."))

        approved_by_link = process.approved_by.a_link
        approved_time = process.approved_time.strftime("%Y-%m-%d")

        summary_text = " ".join(summary_bits)

        if process.rt_ticket is not None:
            rt_link = mark_safe(f'<a href="https://rt.debian.org/{process.rt_ticket}">#{process.rt_ticket}</a>')
            summary_text += _(" RT ticket: {}")
            summary_html = format_html(summary_text, approved_by_link, approved_time, rt_link)
        else:
            summary_html = format_html(summary_text, approved_by_link, approved_time)
    elif process.frozen_by:
        frozen_by_link = process.frozen_by.a_link
        frozen_time = process.frozen_time.strftime("%Y-%m-%d")
        summary = _("Frozen for review")
        summary_html = format_html(_("Frozen by {} on {}"), frozen_by_link, frozen_time)
    elif not rnok or rnok == [rapproval]:
        if rapproval.approved_by:
            summary = _("Waiting for DAM feedback")
            summary_html = escape(
                    _("Inconsistent state: has approval statement but is not marked frozen for review or approved"))
        else:
            summary = _("Waiting for review")
            summary_html = escape(summary)
    elif am_assignment is not None:
        if am_assignment.paused:
            summary = _("AM Hold")
        else:
            summary = _("AM")
        summary_html = escape(summary)
    else:
        summary = _("Collecting requirements")
        summary_html = escape(summary)

    return {
        "requirements": requirements,
        "requirements_sorted": sorted(
            list(requirements.values()), key=lambda x: REQUIREMENT_TYPES_DICT[x.type].sort_order),
        "requirements_ok": sorted(rok, key=lambda x: REQUIREMENT_TYPES_DICT[x.type].sort_order),
        "requirements_missing": sorted(rnok, key=lambda x: REQUIREMENT_TYPES_DICT[x.type].sort_order),
        "log_first": log[0] if log else None,
        "log_last": log[-1] if log else None,
        "log": log,
        "advocates": sorted(advocates, key=lambda x: x.ldap_fields.uid),
        "summary": summary,
        "summary_html": summary_html,
    }


class VisitProcessMixin(VisitPersonMixin):
    """
    Visit a person process. Adds self.person, self.process and
    self.visit_perms with the permissions the visitor has over the person
    """

    def get_person(self):
        return self.process.person

    def get_visit_perms(self):
        return self.process.permissions_of(self.request.user)

    def get_process_menu_entries(self):
        res = super().get_process_menu_entries()
        if self.request.user.is_authenticated:
            if self.request.user.is_superuser:
                res.append(NavLink(self.process.get_admin_url(), _("Admin process"), "microchip"))
            if self.process.applying_for == "dd_e":
                res.append(NavLink(
                    reverse("mia:wat_ping", kwargs={"key": self.person.lookup_key}), _("Resend WAT ping"), "heartbeat"))
                res.append(NavLink(reverse("mia:wat_remove", kwargs={"pk": self.process.pk}), _("WAT remove")))
            if "proc_close" in self.visit_perms:
                res.append(NavLink(
                    reverse("process_cancel", kwargs={"pk": self.process.pk}), _("Cancel"), "remove"))
        return res

    def get_process(self):
        return get_object_or_404(pmodels.Process.objects.select_related("person"), pk=self.kwargs["pk"])

    def load_objects(self):
        self.process = self.get_process()
        super().load_objects()

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        ctx["process"] = self.process
        ctx["wikihelp"] = "https://wiki.debian.org/nm.debian.org/Process"
        return ctx

    def compute_process_status(self):
        return compute_process_status(self.process, self.request.user, self.visit_perms)


class RequirementMixin(VisitProcessMixin):
    # Requirement type. If not found, check self.kwargs["type"]
    type = None
    wikihelp = {
        "intent": "https://wiki.debian.org/DebianDeveloper/JoinTheProject/NewMember/IntentStep",
        "sc_dmup": "https://wiki.debian.org/DebianDeveloper/JoinTheProject/NewMember/CommitmentsStep",
        "advocate": "https://wiki.debian.org/DebianDeveloper/JoinTheProject/NewMember/AdvocacyStep",
        "keycheck": "https://wiki.debian.org/DebianDeveloper/JoinTheProject/NewMember/IdentificationStep",
        "am_ok": "https://wiki.debian.org/DebianDeveloper/JoinTheProject/NewMember/ApplicationManagerStep",
    }

    def get_requirement_type(self):
        if self.type:
            return self.type
        else:
            return self.kwargs.get("type", None)

    def get_requirement(self):
        process = get_object_or_404(pmodels.Process, pk=self.kwargs["pk"])
        return get_object_or_404(pmodels.Requirement, process=process, type=self.get_requirement_type())

    def get_process_menu_entries(self):
        res = super().get_process_menu_entries()
        if self.request.user.is_staff:
            res.append(NavLink(self.requirement.get_admin_url(), _("Admin requirement"), "microchip"))
        return res

    def get_visit_perms(self):
        return self.requirement.permissions_of(self.request.user)

    def get_process(self):
        return self.requirement.process

    def load_objects(self):
        self.requirement = self.get_requirement()
        super(RequirementMixin, self).load_objects()

    def get_context_data(self, **kw):
        ctx = super(RequirementMixin, self).get_context_data(**kw)
        ctx["now"] = now()
        ctx["requirement"] = self.requirement
        ctx["type"] = self.requirement.type
        ctx["type_desc"] = pmodels.REQUIREMENT_TYPES_DICT[self.requirement.type].desc
        ctx["explain_template"] = "process/explain_statement_" + \
            self.requirement.type + ".html"
        ctx["status"] = self.requirement.compute_status()
        ctx["wikihelp"] = self.wikihelp.get(self.requirement.type)
        return ctx


class StatementMixin(RequirementMixin):
    def load_objects(self):
        super(StatementMixin, self).load_objects()
        if "st" in self.kwargs:
            self.statement = get_object_or_404(
                pmodels.Statement, pk=self.kwargs["st"])
            if self.statement.requirement != self.requirement:
                raise PermissionDenied
        else:
            self.statement = None

    def get_context_data(self, **kw):
        ctx = super(StatementMixin, self).get_context_data(**kw)
        ctx["fpr"] = self.request.user.fpr
        ctx["keyid"] = self.request.user.fpr[-16:]
        ctx["statement"] = self.statement
        ctx["now"] = now()
        return ctx
