from __future__ import annotations
from typing import Optional
from django.utils.timezone import now
from . import models as pmodels
from . import ops as pops
import datetime


def ping_stuck_processes(stuck_cutoff, audit_author, logdate=None):
    from .email import ping_process
    if logdate is None:
        logdate = now()

    for process in pmodels.Process.objects.in_early_stage():
        already_pinged = False
        entry = None
        for idx, entry in enumerate(process.log.order_by("logdate")):
            if entry.action == "ping":
                already_pinged = True
        if entry is not None and entry.logdate > stuck_cutoff:
            continue

        # get missing requirements for process in order
        # to be able to issue a more meaningful ping message

        missing_requirements = []
        # additional information appended to ping mail -
        # keycheck does not count as missing requirement here, but is often misinterpreted,
        # so append a message, if it is not approved yet.
        keycheck_message = ""
        # an advocate might take longer than one week to react
        # and the applicant might not be able to influence that,
        # so append a message, that a closed process can be reopened.
        advocate_message = ""

        for r in process.requirements.all():
            if r.type == "advocate" or r.type == "intent" or r.type == "sc_dmup":
                if r.approved_by is None:
                    missing_requirements.append(r)
            # add additional messages for missing advocate and keycheck
            if r.type == "keycheck" and r.approved_by is None:
                keycheck_message = f"""-> Don't worry about {r.type_desc} -
   it's not taken into regard here, because it needs manual approval.
"""
            if r.type == "advocate" and r.approved_by is None:
                advocate_message = """\
-> If you have an advocate that just needs some more time to react,
   the process can be reopened.
"""
        # not having any missing requirements here would be an error,
        # but let's don't have it look weird in the mail and omit the additional text
        if len(missing_requirements) <= 0:
            msg_str = ""
        else:
            # differentiating singular/plural
            if len(missing_requirements) < 2:
                req = "requirement isn't fulfilled"
            else:
                req = "requirements aren't fulfilled"
            msg_str = f"""
This message was triggered because the following {req}:
"""
            for r in missing_requirements:
                msg_str = f"{msg_str}- {r.type_desc}\n"

        if not already_pinged:
            # Detect first instance of processes stuck early: X days from
            # last log, no previous ping message
            msg_str = f"""{msg_str}
If nothing happens, the process will be automatically closed a week from now.
{keycheck_message}{advocate_message}"""

            ping_process(audit_author, process, message=msg_str)
            process.add_log(audit_author, "looks stuck, pinged",
                            action="ping", logdate=logdate)
        else:
            # Detect second instance: X days from last log, no
            # intent/advocate/sc_dmup, a previous ping message
            msg_str = f"""{msg_str}
A week has passed from the last ping with no action, I'll now close the
process. Feel free to reapply in the future.
{advocate_message}
"""
            ping_process(audit_author, process, message=msg_str)
            process.add_log(audit_author, "closing for inactivity",
                            action="proc_close", logdate=logdate, is_public=True)
            process.closed_by = audit_author
            process.closed_time = logdate
            process.save()


def consider_submitting_rt_ticket(send_threshold: datetime.datetime, process: pmodels.Process):
    # The processes here have an approval requirement per the request done
    # to fetch them. Hence, get will return.
    appr = process.requirements.get(type="approval")

    approver = appr.approved_by.am_or_none
    if not approver or not approver.is_fd:
        # Skip processes whose approval is not approved by someone who is FD
        return

    # Compute when FD or DAM last approved the statement
    last_approved_by_fd: Optional[pmodels.Statement] = None
    last_approved_by_dam: Optional[pmodels.Statement] = None

    # Extend with the dates of the last approval statements filed by FDs and DAMs
    for statement in appr.statements.order_by("uploaded_time").all():
        am = statement.fpr.person.am_or_none
        if am is None:
            continue

        if am.is_dam:
            last_approved_by_dam = statement
        elif am.is_fd:
            last_approved_by_fd = statement

    if last_approved_by_dam:
        send_rt_ticket(process, last_approved_by_dam)
        return

    # This could be optimized out by only looping over the approval
    # statement posterior to the latest comment on the approval
    # requirement, but as we ignore comments when an approval statement
    # is made by dam, and as premature optimization is root of a lot of
    # pain in the ass, and as we do not expect a bazillion of
    # statements and logs in a process, I'd like to say meh.
    if last_approved_by_fd:
        latest_log_comment = process.log.order_by('-logdate').first()

        # If there is a comment filed after FD has approved the process, abort
        if latest_log_comment and last_approved_by_fd.uploaded_time < latest_log_comment.logdate:
            return

        if last_approved_by_fd.uploaded_time <= send_threshold:
            send_rt_ticket(process, last_approved_by_fd)


def submit_rt_ticket_for_fd_approved_processes(approval_to_rt_delay: datetime.timedelta):
    """
    Create a RT ticket for all processes approved by a FD member, older than
    approval_to_rt_delay, without a RT ticket already
    """
    send_threshold = now() - approval_to_rt_delay

    # Build a QuerySet matching processes that have their approval requirement
    # satisfied, but have not had an RT ticket assigned yet
    reqs = pmodels.Requirement.objects.filter(type="approval", approved_by__isnull=False)
    processes = pmodels.Process.objects.filter(
        closed_time__isnull=True, approved_by__isnull=False, rt_ticket__isnull=True,
        requirements__in=reqs).distinct()

    for process in processes:
        consider_submitting_rt_ticket(send_threshold, process)


def send_rt_ticket(process, statement):
    """
    Actually sends a rt ticketa for a process
    """

    op = pops.ProcessRT(
        process=process,
        audit_author=statement.fpr.person,
        audit_notes="Opened RT ticket",
    )
    op.rt_text = statement.statement
    op.execute()
