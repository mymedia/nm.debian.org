from django.views.generic import View, TemplateView
from django.views.generic.edit import FormView, DeleteView
from django.shortcuts import redirect, get_object_or_404
from django import forms, http
from django.core.exceptions import PermissionDenied
from backend.mixins import VisitPersonMixin
from django.db import transaction
import backend.models as bmodels
import fprs.forms as fforms
import fprs.ops as fops
from nm2.lib.forms import BootstrapAttrsMixin


class NewFingerprintForm(BootstrapAttrsMixin, forms.ModelForm):
    class Meta:
        model = bmodels.Fingerprint
        fields = ["fpr"]


class PersonFingerprints(VisitPersonMixin, FormView):
    template_name = "fprs/list.html"
    require_visit_perms = "edit_fpr"
    form_class = NewFingerprintForm

    @transaction.atomic
    def form_valid(self, form):
        fpr = form.save(commit=False)
        fpr.person = self.person
        fpr.is_active = True
        fpr.save(audit_author=self.request.user, audit_notes="added new fingerprint")
        # Ensure that only the new fingerprint is the active one
        self.person.fprs.exclude(pk=fpr.pk).update(is_active=False)
        return redirect("fprs:person_list", key=self.person.lookup_key)


class FingerprintMixin(VisitPersonMixin):
    def pre_dispatch(self):
        super(FingerprintMixin, self).pre_dispatch()
        self.fpr = get_object_or_404(bmodels.Fingerprint, fpr=self.kwargs["fpr"])
        if self.fpr.person.pk != self.person.pk:
            raise PermissionDenied

    def get_context_data(self, **kw):
        ctx = super(FingerprintMixin, self).get_context_data(**kw)
        ctx["fpr"] = self.fpr
        ctx["keyid"] = self.fpr.fpr[-16:]
        return ctx


class FingerprintEndorsementMixin(FingerprintMixin):
    def load_objects(self):
        super().load_objects()
        if "endorsement" in self.kwargs:
            self.endorsement = get_object_or_404(bmodels.FingerprintEndorsement, pk=self.kwargs["endorsement"])
        else:
            self.endorsement = None

    def get_object(self):
        return self.endorsement

    def get_context_data(self, **kw):
        ctx = super(FingerprintMixin, self).get_context_data(**kw)
        ctx["fpr"] = self.fpr
        ctx["keyid"] = self.fpr.fpr[-16:]
        ctx["endorsement"] = self.endorsement
        ctx["endorsing_fpr"] = self.request.user.fpr
        return ctx


class ViewKeyEndorsements(FingerprintMixin, TemplateView):
    """Class to allow the view of a fingerprint's endorsement"""
    template_name = "fprs/view_endorsements.html"

    def get_context_data(self, **kw):
        ctx = super(FingerprintMixin, self).get_context_data(**kw)
        ctx["endorsements"] = self.fpr.received_endorsements.all()
        return ctx


class EndorseKey(FingerprintEndorsementMixin, FormView):
    form_class = fforms.EndorsementForm
    require_visit_perms = "endorse_key"
    template_name = "fprs/endorse_key.html"

    def get_form_kwargs(self):
        kw = super().get_form_kwargs()
        kw["endorsing_fpr"] = self.request.user.fpr
        return kw

    @transaction.atomic
    def form_valid(self, form):
        endorsement, plaintext = form.cleaned_data["text"]
        op = fops.FingerprintEndorsementAdd(
            audit_author=self.request.user,
            fingerprint=self.fpr,
            endorsing_fpr=self.request.user.fpr,
            endorsement=endorsement)
        op.execute(self.request)
        return redirect(self.fpr.get_absolute_url())


class ViewRawKeyEndorsement(FingerprintEndorsementMixin, View):
    def get(self, request, *args, **kw):
        return http.HttpResponse(self.endorsement.text, content_type="text/plain")


class DeleteKeyEndorsement(FingerprintEndorsementMixin, DeleteView):
    form_class = fforms.EndorsementDeletionForm
    template_name = "fprs/delete_endorsement.html"

    def check_permissions(self):
        super().check_permissions()
        if not (self.endorsement.author == self.request.user or self.request.user.is_superuser):
            raise PermissionDenied

    def delete(self, request, *args, **kwargs):
        op = fops.FingerprintEndorsementRemove(
            endorsement=self.endorsement,
            audit_author=self.request.user,
        )
        op.execute(self.request)
        return redirect(self.fpr.get_absolute_url())


class SetActiveFingerprint(FingerprintMixin, View):
    require_visit_perms = "edit_fpr"

    @transaction.atomic
    def post(self, request, *args, **kw):
        # Set all other fingerprints as not active
        for f in self.person.fprs.filter(is_active=True).exclude(pk=self.fpr.pk):
            f.is_active = False
            f.save(audit_author=self.request.user, audit_notes="activated fingerprint {}".format(self.fpr.fpr))

        # Set this fingerprint as active
        if not self.fpr.is_active:
            self.fpr.is_active = True
            self.fpr.save(audit_author=self.request.user, audit_notes="activated fingerprint {}".format(self.fpr.fpr))

        return redirect("fprs:person_list", key=self.person.lookup_key)
